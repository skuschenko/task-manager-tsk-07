package com.tsc.skuschenko.tm.model;

import com.tsc.skuschenko.tm.constant.ArgumentConst;
import com.tsc.skuschenko.tm.constant.TerminalConst;

public class Command {

    public static final  Command ABOUT= new Command(
            TerminalConst.TM_ABOUT,
            ArgumentConst.ARG_ABOUT,
             "show about information"
    );

    public static final  Command HELP= new Command(
            TerminalConst.TM_HELP,
            ArgumentConst.ARG_HELP,
            "show help information"
    );

    public static final  Command VERSION= new Command(
            TerminalConst.TM_VERSION,
            ArgumentConst.ARG_VERSION,
            "show version application"
    );

    public static final  Command EXIT= new Command(
            TerminalConst.TM_EXIT,
            null,
            "close program"
    );

    public static final  Command INFO= new Command(
            TerminalConst.TM_INFO,
            ArgumentConst.ARG_INFO,
            "close program"
    );

    @Override
    public String toString() {
        String result="";
        if (name != null && !name.isEmpty())result+=name;
        if (arg != null && !arg.isEmpty())result+=" ["+arg+"]";
        if (description != null && !description.isEmpty())result+=" - "+description;
        return result;
    }

    private String arg = "";

    private String name = "";

    private  String description="";

    public Command( String name, String arg,String description) {
        this.arg = arg;
        this.name = name;
        this.description = description;
    }

    public Command() {
    }

    public String getArg() {
        return arg;
    }

    public void setArg(String arg) {
        this.arg = arg;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
